let assert = require('assert')
let http = require('http')
let { parse } = require('set-cookie-parser')
let { client } = require('websocket')
let { WebSocketClient, defer } = require('../src/ws')
let BrokerClient = require('../src/broker-client')
let grcpBrokerClient = require('../src/broker-grpc-client')
const grpc = require('@grpc/grpc-js')

process.on('uncaughtException', err=>console.error(err))

const PORT = 1234;
const URL = `http://localhost:${PORT}`;
const PEER_ID_COOKIE_NAME = 'pid';
const PEER_POOL_SIZE = 3;
const PEER_DEFAULT_TOPIC_NAME = 'foo';
const PEER_DEFAULT_SUBSCRIPTION_NAME = 'worker';

var socket = null;
var upgradeHead = null;
var headers = null;
var peerId = null;
var endpoint = null;
var pool = [ ];
var broker = null;
var executiveProducer = defer()
var pinId = null;

///TODO
// describe https
// describe acme-challenge
// describe mDNS discovery

describe(`HTTP Server`, ()=>{

    describe(`Response`, ()=>{

        it(`should response with correct CORS headers`, ()=>{
            let requestHeaders = [ 'X-Some-Additional-Header', 'Foo-Bar-Header' ];
            let options = {
                method: 'OPTIONS',
                headers: {
                    'Connection': 'close',
                    'Access-Control-Request-Method': 'PUT',
                    'Access-Control-Request-Headers': requestHeaders.join(','),
                    'Origin': URL,
                }
            }
            return new Promise((resolve, reject)=>{
                let req = http.request(URL+'/', options)
                req.on('response', res=>{
                    let checkHeaders = [
                        'access-control-allow-origin',
                        'access-control-allow-methods',
                        'access-control-allow-headers',
                        'access-control-expose-headers',
                    ];
                    if (checkHeaders.map(key=>key in res.headers).includes(false) == false) {
                        if (res.headers['access-control-allow-methods']==='PUT' && res.headers['access-control-allow-origin']===URL) {
                            let isAllowHeaders = res.headers['access-control-allow-headers'].split(/, */).map(val=>requestHeaders.includes(val)).includes(false) == false;
                            let isExposeHeaders = res.headers['access-control-expose-headers'].split(/, */).map(val=>requestHeaders.includes(val)).includes(false) == false;
                            if (isAllowHeaders && isExposeHeaders)
                                resolve()
                        }
                    }
                    reject(new Error(`Unexpected response headers: ${URL}\n ${JSON.stringify(res.headers, ' ', '\t')} `))
                })
                req.end()
            })
        })
    })
})

describe(`WebSocket Server`, ()=>{

    describe(`Request`, ()=>{

        it(`should handle http upgrade request`, ()=>{
            let options = {
                method: 'GET',
                headers: {
                    'Connection': 'Upgrade',
                    'Upgrade': 'websocket',
                    'Sec-WebSocket-Version': '13',
                    'Sec-WebSocket-Key': 'SGVsbG8sIHdvcmxkIQ==',
                }
            }
            return new Promise((resolve, reject)=>{
                let req = http.request(URL, options)
                req.end()
                req.on('response', msg=>{
                    reject(new Error(`unexpected response status: ${msg.statusCode} ${msg.statusMessage} ${req.method} ${URL}`))
                })
                req.on('upgrade', (res, sock, head)=>{
                    socket = sock;
                    upgradeHead = head;
                    headers = { ...res.headers };
                    resolve()
                })
            })
        })
    })

    describe(`Response`, ()=>{

        it(`should set-cookie "pid" with a peer id value`, (done)=>{
            assert.strictEqual(typeof(headers), 'object', `unxpected response headers format, must be an Object`)
            assert.strictEqual('set-cookie' in headers, true, `Set-Cookie not found in the response headers`)
            assert.strictEqual(Array.isArray(headers['set-cookie']), true, 'unexpected Set-Cookie header value format, must be an array of cookie objects')
            
            let cookie = headers['set-cookie'].find(c=>c.includes(PEER_ID_COOKIE_NAME+'='))
            assert.strictEqual(!!cookie, true, `not found cookie value with name "${PEER_ID_COOKIE_NAME}"`)
            
            let [ pid ] = parse(cookie)
            if (pid.value.length >= 10) {
                peerId = pid.value;
                endpoint = URL.replace(/^http/, 'ws')+`/${peerId}`;
                done()
            } else {
                done(new Error(`unexpected peer id value length: ${pid.value.length}`))
            }
        })
    })
})

///TODO
// it should be able to count a connection attempts
// it should be able to reconnect if was disconnected from client endpoint
// it should change size of connections pool from server requirements

describe(`WebSocket Client`, ()=>{

    describe(`Pool of connections`, ()=>{
        it(`should open ${PEER_POOL_SIZE} WebSocket connection(s)`, ()=>{
            pool = new WebSocketClient(endpoint)

            let c = new client()
            c.socket = socket;
            c.firstDataChunk = upgradeHead;
            c.on('connect', conn=>pool.addConnection(conn))
            c.succeedHandshake()
            
            return pool.makeFailoverPool(PEER_POOL_SIZE)
        })
    })

    describe(`JSON-RPC API`, ()=>{

        it(`should write a text message`, ()=>{
            return pool.write('hello')
        })

        it(`should send a raw Object as JSON message`, ()=>{
            return pool.send({ value:'hello' })
        })

        it(`should call not implemented method and receive unexpected result message`, done=>{
            pool.call('foo', [ 'a', 'b' ]).then(r=>{
                if (r.result)
                    done()
                else
                    done(new Error(`Unexpected result message: ${JSON.stringify(r)}`))
            }, done)
        })

        it(`should call implemented method and receive expected result message`, done=>{
            let ts = (new Date()).toUTCString()
            pool.call('hello', [ ts ]).then(msg=>{
                if (msg.result[0] === ts)
                    done()
                else
                    done(new Error(`Unexpected result: ${JSON.stringify(msg)}`))
            }, done)
        })
    })
})

describe(`Broker JSON-RPC API`, ()=>{

    describe(`Producer`, ()=>{

        it(`should not be available to emit a messages`, done=>{
            pool.call('emit', { topic:'foo', data:[420] }).then(msg=>{
                if (msg.result.isOk == false) {
                    done()
                } else {
                    done(new Error(`Unexpected result message: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should add a producer without specify a name`, done=>{
            pool.call('producer', { topic:'foo' }).then(({result})=>{
                if (result.isOk && result.name === peerId) {
                    done()
                } else {
                    done(new Error(`Unxpected result: ${JSON.stringify(result)}`))
                }
            })
        })

        it(`should add a producer with specified name`, done=>{
            pool.call('producer', { topic:'foo', name:'player' }).then(msg=>{
                if (msg.result.isOk) {
                    done()
                } else {
                    done(new Error(`Unexpected result message: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should be available to emit a messages with specified topic`, done=>{
            pool.call('emit', { topic:'foo', data:[420] }).then(msg=>{
                if (msg.result.isOk) {
                    done()
                } else {
                    done(new Error(`Unexpected result message: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should be available to emit a messages with specified topic and producer name`, done=>{
            pool.call('emit', { topic:'foo', data:[(new Date()).toUTCString()], producer:'player' }).then(msg=>{
                if (msg.result.isOk) {
                    done()
                } else {
                    done(new Error(`Unexpected result message: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should not be available to emit a messages as producer "bar"`, done=>{
            pool.call('emit', { topic:'foo', data:[420], producer:'bar' }).then(msg=>{
                if (msg.result.isOk == false) {
                    done()
                } else {
                    done(new Error(`Unexpected result message: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should not be available to emit a messages on topic "bar"`, done=>{
            pool.call('emit', { topic:'bar', data:[420] }).then(msg=>{
                if (msg.result.isOk == false) {
                    done()
                } else {
                    done(new Error(`Unexpected result message: ${JSON.stringify(msg)}`))
                }
            })
        })
    })

    describe(`Consumer`, ()=>{

        it(`should add subscription without specify a name`, done=>{
            pool.call('subscribe', { topic:'foo' }).then(msg=>{
                if (msg.result.isOk == true && msg.result.name===peerId) {
                    done()
                } else {
                    done(new Error(`Unexpected result message: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should add subscription with specified name`, done=>{
            pool.call('subscribe', { topic:'foo', name:'worker' }).then(msg=>{
                if (msg.result.isOk == true && msg.result.name==='worker') {
                    done()
                } else {
                    done(new Error(`Unexpected result message: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should receive a message for each subscription`, done=>{
            pool.delegate.on('data', msg=>{
                let { topic, subscriptions } = msg.params;
                if (topic === 'foo' && Array.isArray(subscriptions)) {
                    if (subscriptions.indexOf(peerId) >=0 && subscriptions.indexOf('worker')>=0) {
                        done()
                        return
                    }
                }
                done(new Error(`Unexpected topic message: ${JSON.stringify(msg)}`))
            })
            pool.call('emit', { topic:'foo', data:[ 'bar' ] }).then(msg=>{
                if (!msg.result.isOk || msg.result.dest.indexOf(peerId)<0)
                    done(new Error(`Unexpected producer emit result: ${JSON.stringify(msg)}`))
            })
        })

        it(`should remove subscription without specify a name`, done=>{
            pool.call('unsubscribe', { topic:'foo' }).then(msg=>{
                if (msg.result.isOk == true) {
                    done()
                } else {
                    done(new Error(`Unexpected result message: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should remove subscription with specified name`, done=>{
            pool.call('unsubscribe', { topic:'foo', name:'worker' }).then(msg=>{
                if (msg.result.isOk == true) {
                    done()
                } else {
                    done(new Error(`Unexpected result message: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should not available to remove not exists subscription name`, done=>{
            pool.call('unsubscribe', { topic:'foo', name:'some-subscription' }).then(msg=>{
                if (msg.result.isOk == false) {
                    done()
                } else {
                    done(new Error(`Unexpected result message: ${JSON.stringify(msg)}`))
                }
            })
        })
    })
})

///TODO
// describe broker release method

describe(`BrokerClient API`, ()=>{
    
    describe(`open`, ()=>{
        it(`should create a BrokerClient instance and open a pool of connections`, done=>{  
            broker = new BrokerClient(URL)
            broker.open(PEER_POOL_SIZE).then(()=>{
                done()
            })
        })
    })

    describe(`subscribe`, ()=>{
        it(`should make able to receive a message from specified topic`, ()=>{
            let done = defer()
            let result = defer()

            broker.subscribe('foo', async msg=>{
                let { dest } = await result;
                // console.log(msg)
                // console.log(dest)
                if (msg.params.producer === peerId) {
                    if (dest.indexOf(msg.params.peer)>=0) {
                        // await broker.unsubscribe('foo')
                        // done()
                        done.fulfill()
                    }
                }
            }).then(({result})=>{
                if (!result.isOk)
                    done.reject(new Error(`Unexpected subscribe result: ${JSON.stringify(msg)}`))
            })

            broker.peer.promise.then(()=>{
                pool.call('emit', { topic:'foo', data:[ 'bar' ] }).then(msg=>{
                    if (msg.result.isOk)
                        result.fulfill(msg.result)
                    else
                        done.reject(new Error(`Unexpected producer emit result: ${JSON.stringify(msg)}`))
                })
            })

            return done
        })
        
        it(`should make able to receive a message from specified topic and subscription`, ()=>{
            let done = defer()
            let result = defer()

            broker.subscribe({ topic:'foo', name:PEER_DEFAULT_SUBSCRIPTION_NAME }, async msg=>{
                let { dest } = await result;
                // console.log(dest)
                // console.log(msg)
                if (msg.params.producer === peerId && msg.params.subscriptions.indexOf(PEER_DEFAULT_SUBSCRIPTION_NAME)>=0) {
                    if (dest.indexOf(msg.params.peer)>=0) {
                        await broker.unsubscribe({ topic:'foo', name:PEER_DEFAULT_SUBSCRIPTION_NAME })
                        // done()
                        done.fulfill()
                    }
                }
            }).then(({result})=>{
                if (!result.isOk)
                    done.reject(new Error(`Unexpected subscribe result: ${JSON.stringify(msg)}`))
            })

            broker.peer.promise.then(()=>{
                pool.call('emit', { topic:'foo', data:[ 'car' ] }).then(msg=>{
                    if (msg.result.isOk)
                        result.fulfill(msg.result)
                    else
                        done.reject(new Error(`Unexpected producer emit result: ${JSON.stringify(msg)}`))
                })
            })

            return done
        })

        it(`should not receive a message from topic without a producer`, done=>{
            let bar = defer()

            broker.subscribe('bar', msg=>{
                bar.fulfill(msg)
                done(new Error(`Unexpected message on topic "bar": ${JSON.stringify(msg)}`))
            }).then(({result})=>{
                if (!result.isOk)
                    done.reject(new Error(`Unexpected subscribe result: ${JSON.stringify(msg)}`))
            })

            broker.peer.promise.then(()=>{
                pool.call('emit', { topic:'foo', data:[ 'var' ] }).then(async msg=>{
                    if (msg.result.isOk) {
                        setTimeout(()=>bar.fulfill(null), 200)
                        if (await bar === null)
                            done()
                    } else {
                        done(new Error(`Unexpected producer emit result: ${JSON.stringify(msg)}`))
                    }
                })
            })
        })
    })

    describe(`unsubscribe`, ()=>{
        it(`should not receive a message from topic without a subscription`, done=>{
            broker.unsubscribe('foo').then(({result})=>{
                if (result.isOk) {

                    pool.call('emit', { topic:'foo', data:[ 'jar' ] }).then(async msg=>{
                        if (msg.result.isOk && msg.result.dest.indexOf(broker.peerId)<0) {
                            done()
                        } else {
                            done(new Error(`Unexpected producer emit destination: ${JSON.stringify(msg)}`))
                        }
                    })

                } else {
                    done(new Error(`Unexpected message on topic "bar": ${JSON.stringify(msg)}`))
                }
            })
        })
    })

    describe(`createProducer`, ()=>{

        it(`should create a producer for specified topic`, done=>{
            broker.createProducer('foo').then(success=>{
                if (!!success) {
                    done()
                } else {
                    done(new Error(`create producer error`))
                }
            })
        })

        it(`should create a producer with specified name and topic`, done=>{
            broker.createProducer({ name:'executive', topic:'foo' }).then(publish=>{
                if (publish) {
                    executiveProducer = publish;
                    done()
                } else {
                    done(new Error(`create executive producer error`))
                }
            }).then()
        })

        it(`should be able to emit a message from producer function`, done=>{
            executiveProducer(new Date(), { value:'bar' }).then(msg=>{
                if (msg.result.isOk == true) {
                    done()
                } else {
                    done(new Error(`Unexpected producer emit result: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should be able to emit a message from broker-client function`, done=>{
            broker.emit('foo', new Date(), { value:'bar' }).then(msg=>{
                if (msg.result.isOk == true) {
                    done()
                } else {
                    done(new Error(`Unexpected producer emit result: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should be able to pin a message for specified topic`, done=>{
            broker.pin('foo', 'bar', { pinned:true }).then(msg=>{
                if (msg.result.isOk && msg.result.id) {
                    pinId = msg.result.id;
                    done()
                } else {
                    done(new Error(`Unexpected producer pin result: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should receive a pinned message since subscribe to the topic`, done=>{
            broker.subscribe('foo', msg=>{
                if (msg.params.id === pinId) {
                    broker.unsubscribe('foo').then(()=>{
                        done()
                    })
                }
            })
        })

        it(`should be able to unpin a message by id for specified topic`, done=>{
            broker.unpin('foo', pinId).then(msg=>{
                if (msg.result.isOk) {
                    done()
                } else {
                    done(new Error(`Unexpected producer unpin result: ${JSON.stringify(msg)}`))
                }
            })
        })

        it(`should not be able to unpin a message for specified topic without valid id`, done=>{
            broker.unpin('foo', 'some-invalid-pin-id').then(msg=>{
                if (msg.result.isOk == false) {
                    done()
                } else {
                    done(new Error(`Unexpected producer unpin result: ${JSON.stringify(msg)}`))
                }
            })
        })
    })

})

describe(`REST API`, ()=>{

    describe(`topics`, ()=>{

        it(`should responed with topics and pinned messages`, ()=>{

            let dfd = defer()

            let url = URL+'/topics';

            let options = {
                method: 'GET',
                headers: {
                    'Connection': 'close',
                }
            }
            return new Promise((resolve, reject)=>{

                broker.pin('foo', 'bar', { someValue:true }).then(msg=>{
                    if (msg.result.isOk) {
                        dfd.fulfill(msg.result)
                    } else {
                        reject(new Error(`Unexpected producer pin result: ${JSON.stringify(msg)}`))
                    }
                })

                let req = http.request(url, options)
                req.on('response', msg=>{

                    if (!(msg.statusCode>=200 && msg.statusCode<=206)) {
                        reject(new Error(`Unexpected response status: ${url} ${msg.statusCode} ${msg.statusMessage}`))
                        return
                    }

                    let buf = [ ];
                    msg.on('data', chunk=>{
                        buf.push(chunk)
                    })
                    msg.on('end', ()=>{

                        dfd.then(pin=>{
                            broker.unpin('foo', pin.id)
                        })

                        let data = Buffer.concat(buf).toString('utf8')
                        try {
                            let topics = JSON.parse(data)
                            if (!('foo' in topics)) {
                                reject(new Error(`Unexpected response: ${url}\n ${data}`))
                                return
                            }
                            dfd.then(pin=>{
                                let msg = topics.foo.find(msg=>msg.params.id===pin.id)
                                if (msg.data === 'bar' && msg.params.someValue===true) {
                                    resolve()
                                } else {
                                    reject(new Error(`Unexpected pinned message: ${url}\n ${JSON.stringify(msg, ' ', '\t')}`))
                                }
                            })
                            return
                        } catch(e) {
                            reject(e)
                        }
                        reject(new Error(`Unexpected response: ${url}\n ${data}`))
                    })                    
                })
                
                dfd.then(()=>{
                    req.end()
                })
            })
        })

        it(`should be able to request a path of keys for a topic pinned message and receive a target value`, ()=>{

            let dfd = defer()

            let ts = Date.now()
            let url = URL+'/topics/foo/0/params/ts';

            let options = {
                method: 'GET',
                headers: {
                    'Connection': 'close',
                }
            }
            return new Promise((resolve, reject)=>{

                broker.pin('foo', 'bar', { ts }).then(msg=>{
                    if (msg.result.isOk) {
                        dfd.fulfill(msg.result)
                    } else {
                        reject(new Error(`Unexpected producer pin result: ${JSON.stringify(msg)}`))
                    }
                })

                let cleanUp = ()=>{
                    dfd.then(pin=>broker.unpin('foo', pin.id))
                }

                let req = http.request(url, options)
                req.on('response', msg=>{

                    if (!(msg.statusCode>=200 && msg.statusCode<=206)) {
                        cleanUp()
                        reject(new Error(`Unexpected response status: ${url} ${msg.statusCode} ${msg.statusMessage}`))
                        return
                    }

                    let buf = [ ];
                    msg.on('data', chunk=>{
                        buf.push(chunk)
                    })
                    msg.on('end', ()=>{
                        cleanUp()

                        let value = Buffer.concat(buf).toString()

                        try {
                            // if (parseInt(value) !== ts) {
                                resolve()
                                return
                            // }
                        } catch(e) {
                            reject(e)
                        }
                        reject(new Error(`Unexpected response: ${url}\n ${buf.toString()}`))
                    })

                })
                
                dfd.then(()=>{
                    req.end()
                })
            })
        })
    })
})

describe(`gRPC API`, ()=>{

    var id = null;
    var topic = 'foo';

    const EMIT_TOTAL_COUNT = 3;

    describe(`Broker`, ()=>{
        
        it(`should receive a peer id`, done=>{
            grcpBrokerClient.open({ }, (err, res)=>{
                if (err) {
                    done(err)
                    return
                }
                if (res.id) {
                    id = res.id;
                    done()
                } else {
                    done(new Error(`Unexpected response: ${res.message}`))
                }
            })
        })
        
        it(`should add a subscription to the topic and receive a pinned message`, done=>{

            let dfd = defer()

            let ts = Date.now()

            broker.pin(topic, 'bar', { ts }).then(msg=>{
                if (msg.result.isOk) {
                    dfd.fulfill(msg.result)
                } else {
                    reject(new Error(`Unexpected producer pin result: ${JSON.stringify(msg)}`))
                }
            })

            dfd.then(pin=>{
            
                let call = grcpBrokerClient.subscribe({ id, topic })
                
                call.on('data', ({ result, payload })=>{
                    
                    if (result) {
                        if (result.isOk == false)
                            done(new Error(`Unexpected subscribe result: ${JSON.stringify(result)}`))
                        return;
                    }

                    try {
                        let msg = JSON.parse(payload)

                        if (msg.params.id === pin.id) {
                            broker.unpin(topic, pin.id).then(()=>{
                                done()
                            })

                            call.cancel()
                        }
                    } catch(e) {
                        done(e)
                        return
                    }

                })
                
                call.on('end', ()=>{

                })
                
                call.on('error', err=>{
                    if (err.code === grpc.status.CANCELLED) { return }
                    done(err)
                })

            })
        })
        
        it(`should add a subscription to the topic and receive ${EMIT_TOTAL_COUNT} messages`, done=>{

            let finish = defer()
            finish.then(r=>done(r))

            let dfd = defer()

            dfd.then(result=>{
                if (result.isOk == false) {
                    finish.fulfill(new Error(`Unexpected subscribe result: ${JSON.stringify(result)}`))
                    return;
                }

                // emit messages to the topic
                let index = 0;
                let tt = setInterval(()=>{
                    broker.emit(topic, ++index)
                    if (index >= EMIT_TOTAL_COUNT)
                        clearInterval(tt)
                }, 200)

            })

            let call = grcpBrokerClient.subscribe({ id, topic })
            
            let totalCount = 0;

            call.on('data', ({ result, payload })=>{
                
                if (result) {
                    dfd.fulfill(result)
                    return;
                }

                // console.log(payload)
                if (++totalCount >= EMIT_TOTAL_COUNT) {
                    finish.fulfill()
                }
            })
            
            call.on('end', ()=>{

            })
            
            call.on('error', err=>{
                finish.fulfill(err)
            })
        })

        it(`should remove a subscription from the topic`, done=>{
            grcpBrokerClient.unsubscribe({ id, topic }, (err, msg)=>{
                if (msg.isOk == true)
                    done()
                else
                    done(new Error(`Unexpected unsubscribe result: ${JSON.stringify(msg)}`))
            })
        })

        it(`should create a producer and be able to emit a messages`, done=>{
            let finish = defer()
            finish.then(r=>done(r))

            let next = defer()

            // to receive target message
            let call = grcpBrokerClient.subscribe({ id, topic })
            call.on('data', msg=>{
                console.log(msg)

                if (msg.type=='result') {
                    if (msg.result.isOk == true) {
                        next.fulfill()
                    } else {
                        finish.fulfill(new Error(`Unexpected subscribe result: ${JSON.stringify(msg)}`))
                    }
                    return
                }

            })
            
            // to emit target message
            grcpBrokerClient.createProducer({ id, topic }, (err, msg)=>{
                console.log(err, msg)
                if (msg.isOk == true) {
                    next.then(()=>{
                        let json = JSON.stringify(['bar'])
                        let buffer = Buffer.from(json)
                        grcpBrokerClient.emit({ id, topic, json, buffer, data:{ value:'bar' }, props:{ ts:Date.now() } }, (err, msg)=>{
                            console.log(err, msg)
                        })
                    })
                } else {
                    finish.fulfill(new Error(`Unexpected create producer result: ${JSON.stringify(msg)}`))
                }
            })
        })
    })

})