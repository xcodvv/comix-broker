const http = require('http')
const https = require('https')
const EventEmitter = require('events')

module.exports = class extends EventEmitter {

    httpServer = null
    handlers = [ ]

    constructor() {
        super()
        
    }

    listen(opt, callback) {
        if (typeof(opt)!=='object') opt = { port:opt }
        
        this.httpServer = http.createServer()
        this.httpServer.listen(opt, ()=>{
            if (callback instanceof Function)
                callback()
        })
        this.httpServer.on('request', this.listener.bind(this))
    }

    tls(opt, callback) {
        if (typeof(opt)!=='object') opt = { port:opt }
        
        this.httpServer = https.createServer()
        this.httpServer.listen(opt)
        this.httpServer.on('request', this.listener.bind(this))
    }

    async listener(req, res) {
        let { handlers } = this;

        req.method = req.method.toLowerCase()

        res.statusCode = 501;
        res.statusMessage = 'Not Implemented';

        for(let i=0; i<handlers.length; ++i) {
            let { exp, callback } = handlers[i];
            let matches = [ ];
            let m = null;
            if (exp.global) {
                while(m = exp.exec(req.url)) {
                    m.shift() //to remove input string
                    matches = matches.concat(m)
                }
            } else if (m = exp.exec(req.url)) {
                m.shift() //to remove input string
                matches = matches.concat(m)
            }
            if (m || matches.length>0) {
                res.statusCode = 200
                res.statusMessage = 'OK';
                let p = callback(req, res, matches)
                if (p && p instanceof Promise) {
                    await p;
                }
                if (res.writableEnded)
                    return; //
            }
        }

        res.end()
    }

    request(url, callback) {
        if (!(callback instanceof Function)) { return }
        
        if (typeof(url)==='string') { url = new RegExp(`^${url.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')}$`) }
        
        if (!(url instanceof RegExp)) { return }
        
        let h = { exp:url, callback }
        
        this.handlers.push(h)

        return h
    }

    setAccessControlAllow(req, res) {
        var headers = [ ];
        var methods = [ ];
        for(let k in req.headers) {
            switch(k.toLowerCase()) {
                case 'access-control-request-headers':
                    headers.push(req.headers[k])
                    break;
                
                case 'access-control-request-method':
                    methods.push(req.headers[k].toUpperCase())
                    break;
            }
        }
        this.setAccessControlHeaders(headers, res)
        this.setAccessControlAllowMethods(methods, res)
        if (req.headers['origin'])
            this.setAccessControlAllowOrigin(req.headers['origin'], res)
    }

    setAccessControlHeaders(headers, res) {
        if (!Array.isArray(headers)) { headers = [ headers ] }
        if (headers.length > 0) {
            res.setHeader('Access-Control-Allow-Headers', headers.join(','))
            res.setHeader('Access-Control-Expose-Headers', headers.join(','))
        }
    }
    
    setAccessControlAllowHeaders(headers, res) {
        if (!Array.isArray(headers)) { headers = [ headers ] }
        if (headers.length > 0)
            res.setHeader('Access-Control-Allow-Headers', headers.join(','))
    }
    
    setAccessControlExposeHeaders(headers, res) {
        if (!Array.isArray(headers)) { headers = [ headers ] }
        if (headers.length > 0)
            res.setHeader('Access-Control-Expose-Headers', headers.join(','))
    }
    
    setAccessControlAllowMethods(methods, res) {
        if (!Array.isArray(methods)) { methods = [ methods ] }
        if (methods.length > 0)
            res.setHeader('Access-Control-Allow-Methods', methods.join(','))
    }
    
    setAccessControlAllowOrigin(value, res) {
        if (value)
            res.setHeader('Access-Control-Allow-Origin', value)
    }

    linkObject(basepath, target, callback) {
        return this.request(new RegExp(`(\/[^\/]+)`, 'g'), (req, res, matches)=>{

            if (matches[0]!==basepath) { return }
        
            let obj = target;
            for(let i=1; i<matches.length; ++i) {
                let k = matches[i].substring(1)
                if (k in obj) {
                    obj = obj[k];
                    if (typeof(obj)!=='object')
                        break;
                }
            }
        
            if (callback instanceof Function) {
                callback(req, res)
            }

            res.statusCode = 200;
            res.end(JSON.stringify(obj))
        })
    }
}