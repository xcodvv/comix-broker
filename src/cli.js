const path = require('path')
const storage = require('./storage')
const crypto = require('crypto')
const EventEmitter = require('events')

storage.open(path.resolve('..', 'data'))

let delegate = new EventEmitter()

process.nextTick(async ()=>{
    let [ ,, method ] = process.argv;
    
    await storage.ready;

    if (delegate.listenerCount(method)) {
        delegate.emit(method, process.argv.slice(3))
    }
})

delegate.on('create', async ([ username ])=>{
    username = username.trim()
    if (username.length === 0) { return }
    let uid = crypto.createHash('sha1').update(username).digest('hex')
    try {
        await storage.put(`uid:${uid}`, username)
        console.log(uid)
        console.log('OK')
    } catch(err) {
        console.log(err)
    }
})