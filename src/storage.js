const leveldown = require('leveldown')
const { defer } = require('./defer')

module.exports = class {

    static ready = defer()

    static async open(path) {
        let dfd = defer()
        this.db = leveldown(path)
        this.db.open({}, ()=>{
            dfd.fulfill(this.db)
            this.ready.fulfill(this.db)
        })
        return dfd
    }

    static async get(key, options) {
        let dfd = defer()
        let db = await this.ready;
        db.get(key, options, (err, value)=>{
            if (err) {
                dfd.reject(err)
            } else {
                dfd.fulfill(value)
            }
        })
        return dfd
    }

    static async put(key, value, options) {
        let dfd = defer()
        let db = await this.ready;
        db.put(key, value, (err)=>{
            if (err) {
                dfd.reject(err)
            } else {
                dfd.fulfill()
            }
        })
        return dfd
    }

}