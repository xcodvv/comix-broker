const path = require('path')
const grpc = require('@grpc/grpc-js')
const protoLoader = require('@grpc/proto-loader')

const gRPC_PORT = 50051;

const PROTO_PATH = path.resolve(__dirname, '..', 'protos/broker.proto')

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
})

const comixPackage = grpc.loadPackageDefinition(packageDefinition).comix;

const client = module.exports = new comixPackage.Broker(`localhost:${gRPC_PORT}`, grpc.credentials.createInsecure())

console.time('grpc')