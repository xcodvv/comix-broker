const path = require('path')
const storage = require('./storage')
const clients = require('./clients')
const broker = require('./broker')

storage.open(path.resolve('.', 'data'))

clients.serve(1234)

broker.enable()

// clients.rest.request('/test', (req, res)=>{
//     // console.log(req.headers)
//     if ('upgrade' in req.headers) { return }
//     clients.rest.setAccessControlAllow(req, res)
//     res.statusCode = 200;
//     res.end()
// })

function exitHandler(options, exitCode) {
    if (options.cleanup) {
        process.emit('cleanup', options)
    }
    if (options.exit) {
        process.exit()
    }
}

process.on('exit', exitHandler.bind(null, { cleanup:true }))
process.on('SIGINT', exitHandler.bind(null, { exit:true }))
process.on('SIGUSR1', exitHandler.bind(null, { exit:true }))
process.on('SIGUSR2', exitHandler.bind(null, { exit:true }))

process.on('uncaughtException', err=>{
    console.error(err)
})

process.stdin.resume()