const { client } = require('websocket')
const clients = require('./clients')
const pool = require('./pool')
const path = require('path')
const grpc = require('@grpc/grpc-js')
const protoLoader = require('@grpc/proto-loader')

const gRPC_PORT = 50051;

const PROTO_PATH = path.resolve(__dirname, '..', 'protos/broker.proto')

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
})

const comixPackage = grpc.loadPackageDefinition(packageDefinition).comix;


module.exports = class {

    static peers = { }
    static producers = { }
    static consumers = { }
    static pinned = { }
    static pinseq = 0;
    static grpcServer = new grpc.Server()


    static enable() {
        let { rest, delegate } = clients;
        let { grpcServer } = this;

        delegate.on('subscribe', this.subscribeMessage.bind(this))
        delegate.on('unsubscribe', this.unsubscribeMessage.bind(this))
        delegate.on('producer', this.producerMessage.bind(this))
        delegate.on('release', this.releaseMessage.bind(this))
        delegate.on('emit', this.emitMessage.bind(this))
        delegate.on('pin', this.pinMessage.bind(this))
        delegate.on('unpin', this.unpinMessage.bind(this))
        delegate.on('open', this.addPeer.bind(this))
        delegate.on('close', this.removePeer.bind(this))


        rest.request('/', (req, res)=>{
            res.statusCode = 200;
            rest.setAccessControlAllow(req, res)
        })

        rest.linkObject('/topics', this.pinned, (req, res)=>{
            rest.setAccessControlAllow(req, res)
        })

        rest.request(/^\/\.well\-known\/acme\-challenge\/(.+)$/i, (req, res, matches)=>{
            rest.setAccessControlAllow(req, res)
            res.statusCode = 200;
            res.end(matches[0])
        })

        grpcServer.addService(comixPackage.Broker.service, {
            
            open: (call, callback)=>{
                let { name } = call.request;
                let id = clients.makeID()
                this.addPeer(id, name)
                callback(null, { isOk:true, id })
            },

            unsubscribe: (call, callback)=>{
                let { id, topic, name } = call.request;

                if (!id || !(id in this.peers)) { return }

                let isOk = this.unsubscribe(topic, id, name)

                callback(null, { isOk })
            },

            subscribe: (call)=>{
                let { id, topic, name } = call.request;

                if (!id || !(id in this.peers)) { return }
                
                let sb = this.addSubscription(topic, id, name, call)

                call.write({ result:{ isOk:!!sb, name:sb } })

                call.on('cancelled', ()=>{
                    call.end()
                    delete this.consumers[topic][sb];
                })

                if (sb) {
                    this.emitSubscriptionMessages(id, topic, sb, call)
                }
            },

            createProducer: (call, callback)=>{
                let { id, topic, name } = call.request;
                
                if (!id || !(id in this.peers)) { return }

                let pd = this.addProducer(topic, id, name, call)

                callback(null, { isOk:!!pd, name:pd })
            },

            emit: (call, callback)=>{
                let { id, topic, name, json, data, buffer,  } = call.request;
                console.log(call.request)
            }
        })

        grpcServer.bindAsync('0.0.0.0:'+gRPC_PORT, grpc.ServerCredentials.createInsecure(), ()=>{
            grpcServer.start()
            console.timeLog('grpc', `gRPC server is listening on port ${gRPC_PORT} since ${(new Date().toUTCString())}`)
        })

        console.time('grpc')
    }

    static addPeer(peerId, name) {
        this.peers[peerId] = {
            id: peerId,
            name,
        }
    }

    static removePeer(peerId, endpoint) {
        for(let topic in this.consumers) {
            if (peerId in this.consumers[topic]) {
                let c = this.consumers[topic][peerId];
                delete this.consumers[topic][peerId];
                c.subscriptions.length = 0;
            }
        }
        for(let topic in this.producers) {
            for(let name in this.producers[topic]) {
                if (peerId in this.producers[topic]) {
                    let p = this.producers[topic][peerId];
                    delete this.producers[topic][peerId];
                }
            }
        }
        if (peerId in this.peers) {
            delete this.peers[peerId];
        }
    }


    static subscribeMessage(msg, peerId) {
        let { id, params } = msg;
        let sb, tp;
        process.nextTick(()=>clients.writeResult(peerId, id, { isOk:!!sb, name:sb }))
        if (Array.isArray(params)) {
            let [ topic, name ] = params;
            sb = this.addSubscription(tp=topic, peerId, name)
        } else if (typeof(params)==='object') {
            let { topic, name } = params;
            sb = this.addSubscription(tp=topic, peerId, name)
        }
        if (sb) {
            setImmediate(()=>{
                this.emitSubscriptionMessages(peerId, tp, sb)
            })
        }
    }

    static addSubscription(topic, peerId, name, call) {
        if (!topic) { return }

        if (!(topic in this.consumers)) {
            this.consumers[topic] = { }
        }
        if (!name) {
            name = peerId;
        }

        if (!(peerId in this.consumers[topic])) {
            this.consumers[topic][peerId] = { id:peerId, call, subscriptions:[ ] }
        }

        if (this.consumers[topic][peerId].subscriptions.includes(name)) { return }

        this.consumers[topic][peerId].subscriptions.push(name)

        return name
    }

    static emitSubscriptionMessages(peerId, topic, name, call) {
        let { pinned } = this;

        if (!(topic in pinned)) { return }

        if (!name) { name = peerId }
        
        pinned[topic].map(({ data, params })=>{
            let msg = { data, params:{...params} }
            msg.params.subscriptions = [ name ];

            let message = JSON.stringify(msg)

            if (call) {
                call.write({ payload: message })
            } else {
                clients.write(peerId, message)
            }
        })
    }


    static unsubscribeMessage(msg, peerId) {
        let { id, params } = msg;
        let isOk = false;
        process.nextTick(()=>clients.writeResult(peerId, id, { isOk }))
        if (Array.isArray(params)) {
            let [ topic, name ] = params;
            isOk = this.unsubscribe(topic, peerId, name)
        } else if (typeof(params)==='object') {
            let { topic, name } = params;
            isOk = this.unsubscribe(topic, peerId, name)
        }
    }

    static unsubscribe(topic, peerId, name) {
        if (!(topic in this.consumers)) { return false }
        if (!(peerId in this.consumers[topic])) { return false }

        if (!name) {
            name = peerId;
        }
        
        let c = this.consumers[topic][peerId];
        if (name && c.subscriptions.includes(name)) {
            c.subscriptions.splice(c.subscriptions.indexOf(name), 1)
            if (c.subscriptions.length === 0)
                delete this.consumers[topic][peerId];
            return true
        }

        return false
    }


    static producerMessage(msg, peerId) {
        let { id, params } = msg;
        let pd;
        process.nextTick(()=>clients.writeResult(peerId, id, { isOk:!!pd, name:pd }))
        if (Array.isArray(params)) {
            let [ topic, name ] = params;
            pd = this.addProducer(topic, peerId, name)
        } else if (typeof(params)==='object') {
            let { topic, name } = params;
            pd = this.addProducer(topic, peerId, name)
        }
    }

    static addProducer(topic, peerId, name, call) {
        if (!topic) { return }
        
        if (!(topic in this.producers)) {
            this.producers[topic] = { }
        }

        if (!name) { name = peerId }
        if (name in this.producers[topic]) { return name }

        this.producers[topic][name] = { name, id:peerId, call }
        return name
    }

    static releaseMessage(msg, peerId) {
        let { id, params } = msg;
        let isOk = false;
        process.nextTick(()=>clients.writeResult(peerId, id, { isOk, name:sb }))
        if (Array.isArray(params)) {
            let [ topic, producer ] = params;
            isOk = this.releaseProducer(topic, peerId, producer)
        } else if (typeof(params)==='object') {
            let { topic, producer, producers, subscription, subscriptions } = params;

            if (producer)
                isOk = this.releaseProducer(topic, peerId, producer)
            
            if (Array.isArray(producers))
                subscriptions.map(name=>{
                    isOk = this.releaseProducer(topic, peerId, name)
                })

            if (subscription)
                isOk = this.unsubscribe(topic, peerId, subscription)

            if (Array.isArray(subscriptions))
                subscriptions.map(name=>{
                    isOk = this.unsubscribe(topic, peerId, name)
                })
        }
    }

    static releaseProducer(topic, peerId, name) {
        if (!(topic in this.producers)) { return }
        
        if (!name) { name = peerId }
        if (!(name in this.producers[topic])) { return }
        
        delete this.producers[topic][name];
    }


    static emitMessage(msg, peerId) {
        let { id, params } = msg;
        let result;
        process.nextTick(()=>{
            if (result && result.isOk) {
                clients.writeResult(peerId, id, result)
            } else {
                clients.writeResult(peerId, id, { isOk:false })
            }
        })
        if (Array.isArray(params)) {
            let [ topic, data, props ] = params;
            result = this.emit(topic, peerId, data, props)
        } else if (typeof(params)==='object') {
            let { topic, data, props, producer } = params;
            if (typeof(props)!=='object') props = { }
            if (typeof(producer)==='string' && producer.length>0) { props.producer = producer }
            result = this.emit(topic, peerId, data, props)
        }
    }

    static emit(topic, peerId, data, props) {
        if (!(topic in this.producers)) { return }

        if (typeof(props)!=='object') props = { producer: props ? props.toString() : peerId }

        let { producer } = props;

        if (!producer) { producer = peerId }
        if (!(producer in this.producers[topic])) { return }

        let cs = topic in this.consumers ? this.consumers[topic] : { }

        let result = { isOk:true, dest:[ ] }
        let pending = [ ];
        for(let peerId in cs) {
            // if (!(peerId in clients.peers)) {
            //     console.log('peer not in clients list:', peerId)
            //     continue
            // }

            // var totalCount = clients.peers[peerId].pool.filter(conn=>conn.state==='open').length;
            // if (totalCount <= 0) { continue }

            let params = {
                ...props,
                topic,
                producer,
                peer: peerId,
                subscriptions: cs[peerId].subscriptions,
            }

            let message = JSON.stringify({ data, params })

            pending.push({ peerId, message, call:cs[peerId].call })
            
            result.dest.push(peerId)
            
            // cs[peerId].subscriptions.map(name=>{
            //     result.dest.push(peerId)
            //     let message = JSON.stringify({ data, params:{ ...params, topic, producer, peer:peerId, subscription:name } })
            //     pending.push({
            //         message,
            //         peerId,
            //     })
            //     // clients.write(peerId, msg)
            // })
        }

        setImmediate(()=>{
            pending.map(({ peerId, message, call })=>{
                if (call) {
                    call.write({ payload: message })
                } else {
                    clients.write(peerId, message)
                }
            })
        })
        
        return result
    }


    static pinMessage(msg, peerId) {
        let { id, params } = msg;
        
        // let pinName = false;
        // process.nextTick(()=>clients.writeResult(peerId, id, { isOk:!!pinName, name:pinName }))
        
        let result;
        process.nextTick(()=>{
            if (result && result.isOk) {
                clients.writeResult(peerId, id, result)
            } else {
                clients.writeResult(peerId, id, { isOk:false })
            }
        })

        if (Array.isArray(params)) {
            let [ topic, data, props ] = params;
            result = this.pin(topic, peerId, data, props)
        } else if (typeof(params)==='object') {
            let { topic, data, props, producer, id } = params;
            if (typeof(props)!=='object') props = { }
            if (typeof(producer)==='string' && producer.length>0) { props.producer = producer }
            if (typeof(id)==='string' && id.length>0) { props.id = id }
            result = this.pin(topic, peerId, data, props)
        }
    }

    static pin(topic, peerId, data, props) {
        if (!(topic in this.producers)) { return }
        if (typeof(props)!=='object') props = { producer: props ? props.toString() : peerId }

        let { producer, id } = props;

        if (!producer) { producer = peerId; }
        if (!(producer in this.producers[topic])) { return }
        
        if (!(topic in this.pinned)) {
            this.pinned[topic] = [ ];
        }

        if (!id) {
            id = `${++this.pinseq}`;
        } else {
            let n = this.pinned[topic].findIndex(p=>p.params.id===id)
            if (n>=0) {
                this.pinned[topic].splice(n, 1)
            }
        }

        let params = {
            id,
            topic,
            producer,
            peer: peerId,
        }
        let message = { data, params:{...params} }
    
        this.pinned[topic].push({
            data,
            params:{ ...props, ...params },
            // id,
            // message,
            // producer,
            // peerId,
        })

        let result = { isOk:true, id, dest:[ ] }
        
        let pending = [ ];
        if (topic in this.consumers) {
            let cs = this.consumers[topic];
            for(let peerId in cs) {
                params.subscriptions = cs[peerId].subscriptions;
                message = JSON.stringify({ data, params })
                pending.push({ peerId, message })
                result.dest.push(peerId)
                // cs[peerId].subscriptions.map(name=>{
                //     clients.write(peerId, message)
                // })
            }
        }

        setImmediate(()=>{
            pending.map(({ peerId, message, call })=>{
                if (call) {
                    call.write({ payload: message })
                } else {
                    clients.write(dest.peerId, dest.message)
                }
            })
        })

        return result
    }

    static getPinnedMessages(topic) {
        return topic in this.pinned ? this.pinned[topic] : [ ]
    }

    static unpin(topic, id) {
        if (!(topic in this.pinned)) { return false }
        
        let n = this.pinned[topic].findIndex(p=>p.params.id===id)
        if (n>=0) {
            this.pinned[topic].splice(n, 1)
            return true
        }

        return false
    }

    static unpinMessage(msg, peerId) {
        let { id, params } = msg;
        let isOk = false;
        process.nextTick(()=>clients.writeResult(peerId, id, { isOk }))
        if (Array.isArray(params)) {
            let [ topic, id ] = params;
            isOk = this.unpin(topic, id)
        } else if (typeof(params)==='object') {
            let { topic, id, producer, producers, peer, peers, all } = params;

            if (Array.isArray(producers))
                isOk = this.unpinProducers(topic, producers)
            if (producer)
                isOk = this.unpinProducers(topic, [ producer ])

            if (Array.isArray(peers))
                isOk = this.unpinPeers(topic, peers)
            if (peer)
                isOk = this.unpinPeers(topic, [ peer ])
            
            if (id)
                isOk = this.unpin(topic, id)
            
            if (all===true)
                isOk = this.unpinAll(topic)
        }
    }

    static unpinAll(topic) {
        if (!(topic in this.pinned)) { return false }
        
        this.pinned[topic].map(pinned=>{
            delete pinned.data;
            delete pinned.params;
        })
        
        this.pinned[topic].length = 0;
        
        return true
    }

    static unpinProducers(topic, producers) {
        if (!(topic in this.pinned)) { return false }
        if (!Array.isArray(producers)) { return false }
        
        this.pinned[topic].map((pinned, index)=>{
            if (producers.includes(pinned.producer)) {
                delete pinned.data;
                delete pinned.params;
                this.pinned.splice(index, 1)
            }
        })
        
        return true
    }

    static unpinPeers(topic, peers) {
        if (!(topic in this.pinned)) { return false }
        if (!Array.isArray(peers)) { return false }

        this.pinned[topic].map((pinned,index)=>{
            if (peers.includes(pinned.peerId)) {
                delete pinned.data;
                delete pinned.params;
                this.pinned.splice(index, 1)
            }
        })

        return true
    }
}