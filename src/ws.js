const { throws } = require('assert');
let EventEmitter = require('events')
let { client } = require('websocket')

function defer() {
	return new PendingPromise()
}

class PendingPromise {
	constructor() {
		this.promise = new Promise((resolve, reject)=>{
			this.resolve = this.fulfill = resolve;
			this.reject = reject;
		})
		this.then = this.promise.then.bind(this.promise)
	}
}

class WebSocketClient {

    constructor(endpoint) {
        this.endpoint = endpoint;
        this.seq = 0;
        this.poolLenth = 0;
        this.pool = [ ];
        this.pending = { }
        this.handler = { }
        this.promise = defer()
        this.delegate = new EventEmitter()
    }

    emit() {
        return this.delegate.emit.apply(this.delegate, arguments)
    }

    close() {
        this.promise = defer()
        this.pool.filter(c=>c && c.state==='open').map(c=>c.close())
    }

    addConnection(sock) {
        sock.on('close', ()=>{
            //
        })
        sock.on('message', message=>{
            if (message.type === 'utf8') {
                this.handleRawMessage(message.utf8Data)
            } else if (message.type === 'binary') {
                console.log('unexpected binary data', message.binaryData.length, 'bytes')
            }
        })
        this.pool.push(sock)
        this.promise.fulfill()
        return this.pool.length-1
    }

    promiseConnection() {
        return new Promise((resolve, reject)=>{
            let c = new client()
            c.on('connectFailed', reject)
            c.on('connect', resolve)
            c.connect(this.endpoint)
        })
    }

    async connect() {
        let ws = await this.promiseConnection().catch(err=>{
            throw err;
        })
        this.addConnection(ws)
        return ws
    }

    makeFailoverPool(length) {
        if (typeof(length)==='number')
            this.poolLenth = length;

        if (this.pool.length > this.poolLenth) {
            this.pool.splice(0, this.poolLenth-this.pool.length).map(c=>c.close())
            return Promise.all([ ])
        }
        
        let pending = [ ];
        for(let i=this.pool.length; i<this.poolLenth; ++i) {
            pending.push(this.connect())
        }
        
        return Promise.all(pending)
    }

    getSocket() {
        return this.pool.find(c=>c && c.state==='open')
    }

	async write(message) {
        await this.promise;
        let [ conn ] = this.pool;
        if (!conn) { return }
        conn.sendUTF(message)
        return conn
	}

	async send(data) {
        await this.promise;
        let [ conn ] = this.pool;
        if (!conn) { return }
        conn.sendUTF(JSON.stringify(data))
        return conn
	}

	call(method, params) {
        let seq = ++this.seq;
        let dfd = this.pending[seq] = defer()
        this.send({
            id:     seq,
            method: method,
            params: params
        })
        return dfd.promise
	}

	handle(method, callback) {
		let list = this.handler[method];
		if (list == null) {
			this.handler[method] = [callback];
			return
		}
		list.push(callback)
    }
    
    handleRawMessage(message) {
		this.emit('message', message)

		var msg, request, handler;
		try {
            msg = JSON.parse(message)
            if ('data' in msg)
                this.emit('data', msg)
		} catch (err) {
			console.warn("parse incoming message error:", err)
			return
		}

		// Notice
		if (msg.id == void 0 || msg.id == 0) {
			if (msg.method) { 
				handler = this.handler[msg.method]
				if (handler) {
					handler.map(h => h(msg.params, this))
					return
				}
                console.warn("no handler for method:", msg.method)
			}
			return
		}

		request = this.pending[msg.id];
		if (request == null) {
			let seqNum = parseInt(msg.id)
			if (!this.lastSeq || seqNum>this.lastSeq) {
				this.lastSeq = seqNum;
				console.warn("no pending request for:", msg.method, msg.id)
			}
			return
		}

		delete this.pending[msg.id];
		if (msg.error != null) {
			request.reject(msg)
		} else {
			request.resolve(msg)
		}
	}
}

module.exports = {
    WebSocketClient,
    PendingPromise,
    defer,
}