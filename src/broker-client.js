let assert = require('assert')
let { client, connection } = require('websocket')
let { parse } = require('set-cookie-parser')
let { WebSocketClient, defer } = require('./ws')

const PEER_ID_COOKIE_NAME = 'pid';
const PEER_DEFAULT_POOL_SIZE = 3;

module.exports = class {

    url = null
    peerId = null
    peer = null
    topics = { }

    constructor(url) {
        this.peer = new WebSocketClient(null)
        if (typeof(url)==='string')
            this.url = url;//.replace(/^http/, 'ws')
    }

    enable() {
        let { peer, topics } = this;
        peer.delegate.on('data', msg=>{
            let { topic, subscriptions } = msg.params;
            if (topic in topics && Array.isArray(subscriptions)) {
                subscriptions.map(name=>{
                    if (name in topics[topic].handler)
                        topics[topic].handler[name].call(this, msg)
                })
            }
        })
    }

    async open(poolLength=PEER_DEFAULT_POOL_SIZE) {
        let c = new client()

        c.on('connect', conn=>{
            let { headers } = c.response;

            assert.strictEqual('set-cookie' in headers, true, `Set-Cookie not found in the response headers`)
            assert.strictEqual(Array.isArray(headers['set-cookie']), true, 'unexpected Set-Cookie header value format, must be an array of cookie objects')
            
            let cookie = headers['set-cookie'].find(c=>c.includes(PEER_ID_COOKIE_NAME+'='))
            assert.strictEqual(!!cookie, true, `not found cookie value with name "${PEER_ID_COOKIE_NAME}"`)

            let [ pid ] = parse(cookie)
            if (pid.value.length > 0) {
                this.peerId = pid.value;
                this.peer.endpoint = this.url.replace(/^http/, 'ws')+this.peerId;
                this.peer.addConnection(conn)
                this.peer.makeFailoverPool(poolLength)
            } else {
                console.error(new Error(`unexpected peer id value length: ${pid.value.length}`))
            }
        })

        c.connect(this.url)

        this.enable()

        await this.peer.promise
    }

    async subscribe(opt, fn) {
        if (!(fn instanceof Function)) { return }
        if (typeof(opt)!=='object') opt = { topic:opt }

        let { topic, name } = opt;

        if (!topic || typeof(topic)!=='string') { return }

        if (!(topic in this.topics)) {
            this.topics[topic] = { ...opt, handler:{} }
        }

        let msg = await this.peer.call('subscribe', { topic, name })
        if (msg.result.isOk == true) {
            this.topics[topic].handler[msg.result.name] = fn;
        }

        return msg
    }

    async unsubscribe(opt) {
        if (typeof(opt)!=='object') opt = { topic:opt }
        
        let { topic, name } = opt;

        if (!topic || typeof(topic)!=='string') { return }
        if (!(topic in this.topics)) { return }

        let msg = await this.peer.call('unsubscribe', { topic, name })
        if (msg.result.isOk == true) {
            if (msg.result.name in this.topics[topic].handler) {
                delete this.topics[topic].handler[msg.result.name];
            }
        }

        return msg
    }

    async createProducer(opt) {
        if (typeof(opt)!=='object') opt = { topic:opt }
        
        let { topic, name } = opt;

        if (!topic || typeof(topic)!=='string') { return }

        let msg = await this.peer.call('producer', { topic, name })

        if (!msg || !msg.result.isOk) { return }

        name = msg.result.name;

        return (data, props)=>this.peer.call('emit', { producer:name, topic, data, props })
    }

    async releaseProducer(opt) {
        if (typeof(opt)!=='object') opt = { topic:opt }
        
        let { topic, name } = opt;

        if (!topic || typeof(topic)!=='string') { return }

        let msg = await this.peer.call('release', { topic, producer:name })

        if (!msg || !msg.result.isOk) { return }
    }

    async emit(opt, data, props) {
        if (typeof(opt)!=='object') opt = { topic:opt }
        
        let { topic, producer } = opt;

        if (!topic || typeof(topic)!=='string') { return }

        return this.peer.call('emit', { producer, topic, data, props })
    }
    
    async pin(opt, data, props) {
        if (typeof(opt)!=='object') opt = { topic:opt }
        
        let { topic, producer } = opt;

        if (!topic || typeof(topic)!=='string') { return }

        return this.peer.call('pin', { producer, topic, data, props })
    }
    
    async unpin(topic, id) {
        if (!topic || typeof(topic)!=='string') { return }
        if (!id || typeof(id)!=='string') { return }
        return this.peer.call('unpin', { topic, id })
    }

    async unpinAll(topic) {
        if (!topic || typeof(topic)!=='string') { return }
        return this.peer.call('unpin', { topic, all:true })
    }

    async unpinProducers(topic, producers) {
        if (!topic || typeof(topic)!=='string') { return }
        if (!Array.isArray(producers) || producers.length===0) { return }
        return this.peer.call('unpin', { topic, producers })
    }

    async unpinPeers(topic, peers) {
        if (!topic || typeof(topic)!=='string') { return }
        if (!Array.isArray(peers) || peers.length===0) { return }
        return this.peer.call('unpin', { topic, peers })
    }
}