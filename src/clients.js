const http = require('http')
const restapp = require('./restapp')
const pool = require('./pool')
const EventEmitter = require('events')
const WebSocketServer = require('websocket').server;
const defer = require('./defer');

module.exports = class {

    static delegate = new EventEmitter()
    
    static rest = null
    
    static peers = { }
    static pending = { }

    
    static serve(port) {
        console.time('http')
        console.time('ws')

        pool.run()

        this.rest = new restapp()

        this.rest.listen({ port }, ()=>{
            console.timeLog('http', `WebSocket server is listening on port ${port} since ${new Date()}`)
        })
        
        let wss = new WebSocketServer({
            httpServer: this.rest.httpServer,
            autoAcceptConnections: false,
        })

        wss.on('request', req=>{
            pool.job(()=>this.upgradeRequest(req))
        })

        this.delegate.on('hello', (msg, peerId)=>{
            this.writeResult(peerId, msg.id, msg.params)
        })
    }

    static broadcast(msg) {
        let { peers } = this;
        for(var k in peers) {
            let p = peers[k];
            if (p.pool.length>0)
                this.sendJob(k, conn=>conn.sendUTF(msg))
        }
    }

    static write(peerId, msg) {
        let { peers } = this;
        if (!(peerId in peers)) { return }
        this.sendJob(peerId, conn=>conn.sendUTF(msg))
    }

    static writeResult(peerId, id, result) {
        let { pending } = this;
        if (!(peerId in pending) || !(id in pending[peerId])) { return }
        delete pending[peerId][id];
        this.sendJob(peerId, conn=>{
            let { label } = this.peers[peerId];
            let msg = JSON.stringify({ id, result })
            console.timeLog(label, `result ${id}`, msg)
            conn.sendUTF(msg)
        })
    }

    static map(cb) {
        let { peers } = this;
        let q = [ ];
        for(var id in peers) {
            if (!!cb(peers[id]))
                q.push(peers[id])
        }
        return q
    }

    static get(peerId) {
        let { peers } = this;
        if (peerId in peers) {
            return peers[peerId]
        }
    }

    static makeID(length=12) {
        let result = [ ];
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < length; ++i) {
           result.push(characters.charAt(Math.floor(Math.random() * charactersLength)))
        }
        return result.join('')
    }

    static getPeerSocket(peerId) {
        let { peers } = this;
        if (!(peerId in peers)) { return }
        let peer = peers[peerId];
        if (peer.pool.length===0) { return }
        return peer.pool.find(c=>c.state==='open')
        // let [ conn ] = peer.pool;
        // return conn && conn.state==='open' ? conn : null
    }

    static sendJob(peerId, cb) {
        let { peers } = this;
        if (!(peerId in peers)) { return }
        let peer = peers[peerId];
        if (peer.pool.length===0) { 
            console.timeLog('ws', 'no active connections to peer', peerId)
            return
        }
        pool.job(()=>cb(this.getPeerSocket(peerId)))
    }

    static upgradeRequest(req) {

        // console.log(req.httpRequest.headers)
        
        let peerId = this.makeID()
        let location = new URL(req.httpRequest.url, 'http://'+req.httpRequest.headers['host'])

        if (location.pathname.length > 1) {
            let pid = location.pathname.substring(1)
            if (pid in this.peers) {
                peerId = pid;
            } else {
                req.reject(406)
                return
            }
        }
        
        let conn = req.accept(null, null, [{ name:'pid', value:peerId }])
        let endpoint = conn.endpoint = `${conn.socket.remoteAddress}:${conn.socket.remotePort}`;
        let label = `sock[${endpoint}]`;

        if (!(peerId in this.peers)) {
            this.peers[peerId] = { id:peerId, label, pool:[ conn ], totalCount:1, remoteAddress:conn.remoteAddress }

            console.timeLog('ws', 'got peer', peerId, conn.state, 'from', endpoint)
            this.delegate.emit('open', peerId)

        } else {
            this.peers[peerId].pool.push(conn)
        }
        
        let totalCount = this.peers[peerId].pool.filter(conn=>conn.state==='open').length;
        console.timeLog('ws', 'incoming connection', endpoint, conn.state, 'with', peerId, `(${totalCount})`)
        console.time(label)
        
        conn.on('close', (reasonCode, description)=>{
            console.timeLog(label, description, reasonCode, conn.state)
            console.timeEnd(label)
            let totalCount = this.peers[peerId].pool.filter(conn=>conn.state==='open').length;
            console.timeLog('ws', 'peer', peerId, `got ${totalCount} active connection(s)`)
            // if (peerId in this.pending)
                // delete this.pending[peerId];
            if (peerId in this.peers) {
                let n = this.peers[peerId].pool.findIndex(c=>c.endpoint===endpoint)
                if (n >= 0)
                    this.peers[peerId].pool.splice(n, 1)
            }
            if (this.peers[peerId].pool.length === 0) {
                console.timeLog('ws', 'peer', peerId, `gone away`)
                this.delegate.emit('close', peerId, endpoint, reasonCode, description)
                this.peers[peerId].pool.length = 0;
                delete this.peers[peerId];

            }
        })

        conn.on('message', message=>{
            if (message.type === 'utf8') {
                let data;
                try { data = JSON.parse(message.utf8Data) } catch(e) { }
                if (!data) {
                    console.timeLog(label, 'unexpected message:', message.utf8Data)
                    this.sendJob(peerId, conn=>{
                        conn.sendUTF(JSON.stringify({
                            result: {
                                isOk: false,
                                error: 'unexpected message format',
                            }
                        }))
                    })
                    return
                }
                if (!('method' in data)) {
                    console.timeLog(label, 'unexpected data:', message.utf8Data)
                    this.sendJob(peerId, conn=>{
                        conn.sendUTF(JSON.stringify({
                            result: {
                                isOk: false,
                                error: 'unexpected data format',
                            }
                        }))
                    })
                    return
                }
                if (data.id) {
                    if (!this.delegate.listenerCount(data.method)) {
                        console.timeLog(label, 'method not implemented:', message.utf8Data)
                        this.sendJob(peerId, conn=>{
                            conn.sendUTF(JSON.stringify({
                                id: data.id,
                                result: {
                                    isOk: false,
                                    error: 'method not implemented',
                                }
                            }))
                        })
                        return
                    }
                    if (!(peerId in this.pending)) {
                        this.pending[peerId] = { }
                    }  
                    this.pending[peerId][data.id] = conn;
                }
                if (!this.delegate.listenerCount(data.method) || data.method==='close') {
                    console.timeLog(label, 'method not implemented:', message.utf8Data)
                    this.sendJob(peerId, conn=>{
                        conn.sendUTF(JSON.stringify({
                            result: {
                                isOk: false,
                                error: 'method not implemented',
                            }
                        }))
                    })
                    return
                }

                console.timeLog(label, `call ${data.id} "${data.method}"`, JSON.stringify(data.params))

                this.delegate.emit(data.method, data, peerId, endpoint)
            }
            else if (message.type === 'binary') {
                console.timeLog(label, 'got binary data', message.binaryData.length, 'bytes')
                this.sendJob(peerId, conn=>{
                    conn.sendUTF(JSON.stringify({
                        result: {
                            isOk: false,
                            error: 'unexpected binary data',
                        }
                    }))
                    // conn.sendBytes(message.binaryData);
                })
            }
        })
    }
}