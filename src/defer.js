class PendingPromise {
    constructor() {
        this.promise = new Promise((resolve,reject)=>{
            this.fulfill = this.resolve = resolve;
            this.reject = reject;
        })
        this.then = this.promise.then.bind(this.promise)
        this.catch = this.promise.catch.bind(this.promise)
    }
}

module.exports = {
    
    PendingPromise,

    defer() {
        return new PendingPromise()
    }
}