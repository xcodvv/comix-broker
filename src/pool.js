const { defer, PendingPromise } = require('./defer')

module.exports = class {

    static _jobs = [ ]
    static jobs = [ ]
    static groupSize = 10

    static job(cb) {
        if (!(cb instanceof Function)) { return }
        this.jobs.push(cb)
    }

    static do() {
        let len = this.jobs.length;
        if (len === 0) { return }
        let jj = this.jobs.splice(0, this.groupSize)
        for(let i=0; i<len; ++i) {
            if (!jj[i]) { continue }
            jj[i]()
        }
    }

    static run() {
        this.do()
        setTimeout(()=>this.run(), 0)
    }

}